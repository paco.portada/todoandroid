package com.example.todo.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.databinding.ActivityAddBinding;
import com.example.todo.databinding.ActivityUpdateBinding;
import com.example.todo.model.AddResponse;
import com.example.todo.model.Task;
import com.example.todo.network.ApiTokenRestClient;
import com.example.todo.util.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener, Callback<AddResponse> {
    public static final int OK = 1;

    private ProgressDialog progreso;
    SharedPreferencesManager preferences;

    private ActivityUpdateBinding binding;

    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //activity_main.xml -> ActivityMainBinding
        binding = ActivityUpdateBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.accept.setOnClickListener(this);
        binding.cancel.setOnClickListener(this);

        preferences = new SharedPreferencesManager(this);

        Intent i = getIntent();
        task = (Task) i.getSerializableExtra("task");
        binding.textViewId.setText(String.valueOf(task.getId()));
        binding.editText.setText(String.valueOf(task.getDescription()));
    }

    @Override
    public void onClick(View v) {

        String description;

        hideSoftKeyboard();

        if (v == binding.accept) {
            description = binding.editText.getText().toString();
            if (description.isEmpty())
                Toast.makeText(this, "Please, fill the description", Toast.LENGTH_SHORT).show();
            else {
                task.setDescription(description);
                connection(task);
            }
        } else if (v == binding.cancel) {
            finish();
        }
    }

    private void connection(Task task) {
        showMessage(task.getId() + "");
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Site> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        Call<AddResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).updateTask(task, task.getId());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddResponse> call, Response<AddResponse> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            AddResponse addResponse = response.body();
            if (addResponse.getSuccess()) {
                Intent i = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("id", addResponse.getData().getId());
                bundle.putString("description", addResponse.getData().getDescription());
                bundle.putString("createdAt", addResponse.getData().getCreatedAt());
                i.putExtras(bundle);
                setResult(OK, i);
                finish();
                showMessage("Task updated ok: " + addResponse.getData().getDescription());
            } else {
                String message = "Error updating the task";
                if (!addResponse.getMessage().isEmpty()) {
                    message += ": " + addResponse.getMessage();
                }
                showMessage(message);

            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: ");
            Log.e("Error:", response.errorBody().toString());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<AddResponse> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

